package Array;

import java.util.Arrays;

public class Array_Repaso {

	public static void main(String[] args) {

		// inicio de  array
		int numeros[] = new int[10];

		for (int i = 0; i < numeros.length; i++) {
			numeros[i] = 0 + i;

		}
		// pintamos el  array
		System.out.println("Array original");
		System.out.println(Arrays.toString(numeros));

		
		//for para modificar los pares
		for (int i = 0; i < numeros.length; i++) {
			if (i % 2 == 0) {
				numeros[i] = 0;

			}

		}
		//pintamos el  array modificado
		System.out.println("");
		System.out.println("Array modificando los pares");
		System.out.println(Arrays.toString(numeros));
		//contadores para contar los numeros
		int contadorzeros= 0, contador = 0;
		
		//en el if se  cuentan los 0, en el else el resto de numeros
		for (int i = 0; i < numeros.length; i++) {

			if (numeros[i] == 0) {
				contadorzeros++;

			} else {
				contador++;
			}

		}
		//pintamos  el resultado
		System.out.println("");
		System.out.println("El numero de ceros en el array es " + contadorzeros);
		System.out.println("Los numeros diferentes del array son " + contador);
	}

}

//realizado por pablo